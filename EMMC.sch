EESchema Schematic File Version 4
LIBS:beagle-cube-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 8
Title "eMMC v4.3 Interface"
Date "2019-08-03"
Rev "A1"
Comp "Adam Newhouse"
Comment1 "Copyright 2019"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L beagle-cube-rescue:+3V3-power-beagle-cube-rescue #PWR024
U 1 1 5D495F79
P 2500 2550
AR Path="/5D820ACD/5D495F79" Ref="#PWR024"  Part="1" 
AR Path="/5D8BF7DF/5D495F79" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 2500 2400 50  0001 C CNN
F 1 "+3V3" H 2515 2723 50  0000 C CNN
F 2 "" H 2500 2550 50  0001 C CNN
F 3 "" H 2500 2550 50  0001 C CNN
	1    2500 2550
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R1
U 1 1 5D49A4EB
P 2800 3050
AR Path="/5D820ACD/5D49A4EB" Ref="R1"  Part="1" 
AR Path="/5D8BF7DF/5D49A4EB" Ref="R12"  Part="1" 
F 0 "R1" V 2880 3050 50  0000 C CNN
F 1 "10K" V 2800 2975 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2730 3050 50  0001 C CNN
F 3 "~" H 2800 3050 50  0001 C CNN
	1    2800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2950 4800 3000
Wire Wire Line
	4800 3000 5000 3000
NoConn ~ 5000 3150
Wire Wire Line
	4800 2650 4800 2600
Wire Wire Line
	2500 2550 2500 2600
Wire Wire Line
	2800 2900 2800 2600
Connection ~ 2800 2600
Wire Wire Line
	2800 2600 2500 2600
Wire Wire Line
	3000 2900 3000 2600
Connection ~ 3000 2600
Wire Wire Line
	3000 2600 2800 2600
Wire Wire Line
	4800 2600 6450 2600
Wire Wire Line
	6450 2600 6450 3000
Wire Wire Line
	6450 3000 6400 3000
Connection ~ 4800 2600
Wire Wire Line
	6450 3000 6450 3100
Wire Wire Line
	6450 3100 6400 3100
Connection ~ 6450 3000
Wire Wire Line
	6400 3950 6450 3950
Wire Wire Line
	6450 3950 6450 3850
Connection ~ 6450 3100
Wire Wire Line
	6400 3200 6450 3200
Connection ~ 6450 3200
Wire Wire Line
	6450 3200 6450 3100
Wire Wire Line
	6400 3300 6450 3300
Connection ~ 6450 3300
Wire Wire Line
	6450 3300 6450 3200
Wire Wire Line
	6400 3550 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	6450 3550 6450 3300
Wire Wire Line
	6400 3650 6450 3650
Connection ~ 6450 3650
Wire Wire Line
	6450 3650 6450 3550
Wire Wire Line
	6400 3750 6450 3750
Connection ~ 6450 3750
Wire Wire Line
	6450 3750 6450 3650
Wire Wire Line
	6400 3850 6450 3850
Connection ~ 6450 3850
Wire Wire Line
	6450 3850 6450 3750
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR026
U 1 1 5D4E7F1A
P 6450 5600
AR Path="/5D820ACD/5D4E7F1A" Ref="#PWR026"  Part="1" 
AR Path="/5D8BF7DF/5D4E7F1A" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 6450 5350 50  0001 C CNN
F 1 "GND" H 6455 5427 50  0000 C CNN
F 2 "" H 6450 5600 50  0001 C CNN
F 3 "" H 6450 5600 50  0001 C CNN
	1    6450 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4500 6450 4500
Wire Wire Line
	6450 4500 6450 4600
Wire Wire Line
	6400 5550 6450 5550
Connection ~ 6450 5550
Wire Wire Line
	6450 5550 6450 5600
Wire Wire Line
	6400 5450 6450 5450
Connection ~ 6450 5450
Wire Wire Line
	6450 5450 6450 5550
Wire Wire Line
	6400 5350 6450 5350
Connection ~ 6450 5350
Wire Wire Line
	6450 5350 6450 5450
Wire Wire Line
	6400 5250 6450 5250
Connection ~ 6450 5250
Wire Wire Line
	6450 5250 6450 5350
Wire Wire Line
	6400 5150 6450 5150
Connection ~ 6450 5150
Wire Wire Line
	6450 5150 6450 5250
Wire Wire Line
	6400 5000 6450 5000
Connection ~ 6450 5000
Wire Wire Line
	6450 5000 6450 5150
Wire Wire Line
	6400 4900 6450 4900
Connection ~ 6450 4900
Wire Wire Line
	6450 4900 6450 5000
Wire Wire Line
	6400 4800 6450 4800
Connection ~ 6450 4800
Wire Wire Line
	6450 4800 6450 4900
Wire Wire Line
	6400 4700 6450 4700
Connection ~ 6450 4700
Wire Wire Line
	6450 4700 6450 4800
Wire Wire Line
	6400 4600 6450 4600
Connection ~ 6450 4600
Wire Wire Line
	6450 4600 6450 4700
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C8
U 1 1 5D503EA2
P 6450 4200
AR Path="/5D820ACD/5D503EA2" Ref="C8"  Part="1" 
AR Path="/5D8BF7DF/5D503EA2" Ref="C14"  Part="1" 
F 0 "C8" H 6565 4246 50  0000 L CNN
F 1 "100nF" H 6565 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6488 4050 50  0001 C CNN
F 3 "~" H 6450 4200 50  0001 C CNN
	1    6450 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C9
U 1 1 5D5065BA
P 6950 4200
AR Path="/5D820ACD/5D5065BA" Ref="C9"  Part="1" 
AR Path="/5D8BF7DF/5D5065BA" Ref="C15"  Part="1" 
F 0 "C9" H 7065 4246 50  0000 L CNN
F 1 "100nF" H 7065 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 4050 50  0001 C CNN
F 3 "~" H 6950 4200 50  0001 C CNN
	1    6950 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C10
U 1 1 5D506C92
P 7450 4200
AR Path="/5D820ACD/5D506C92" Ref="C10"  Part="1" 
AR Path="/5D8BF7DF/5D506C92" Ref="C16"  Part="1" 
F 0 "C10" H 7565 4246 50  0000 L CNN
F 1 "100nF" H 7565 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 4050 50  0001 C CNN
F 3 "~" H 7450 4200 50  0001 C CNN
	1    7450 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C11
U 1 1 5D5073F4
P 7950 4200
AR Path="/5D820ACD/5D5073F4" Ref="C11"  Part="1" 
AR Path="/5D8BF7DF/5D5073F4" Ref="C17"  Part="1" 
F 0 "C11" H 8065 4246 50  0000 L CNN
F 1 "100nF" H 8065 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7988 4050 50  0001 C CNN
F 3 "~" H 7950 4200 50  0001 C CNN
	1    7950 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C12
U 1 1 5D507B87
P 8450 4200
AR Path="/5D820ACD/5D507B87" Ref="C12"  Part="1" 
AR Path="/5D8BF7DF/5D507B87" Ref="C18"  Part="1" 
F 0 "C12" H 8565 4246 50  0000 L CNN
F 1 "10uF" H 8565 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8488 4050 50  0001 C CNN
F 3 "~" H 8450 4200 50  0001 C CNN
	1    8450 4200
	1    0    0    -1  
$EndComp
Connection ~ 6450 3950
Connection ~ 6450 4500
Wire Wire Line
	6450 3950 6450 4050
Wire Wire Line
	6450 3950 6950 3950
Wire Wire Line
	8450 3950 8450 4050
Wire Wire Line
	7450 3950 7450 4050
Connection ~ 7450 3950
Wire Wire Line
	7450 3950 7950 3950
Wire Wire Line
	6950 3950 6950 4050
Connection ~ 6950 3950
Wire Wire Line
	6950 3950 7450 3950
Wire Wire Line
	7950 4050 7950 3950
Connection ~ 7950 3950
Wire Wire Line
	7950 3950 8450 3950
Wire Wire Line
	6450 4350 6450 4500
Wire Wire Line
	8450 4500 7950 4500
Wire Wire Line
	8450 4350 8450 4500
Wire Wire Line
	6950 4350 6950 4500
Connection ~ 6950 4500
Wire Wire Line
	6950 4500 6450 4500
Wire Wire Line
	7450 4350 7450 4500
Connection ~ 7450 4500
Wire Wire Line
	7450 4500 6950 4500
Wire Wire Line
	7950 4350 7950 4500
Connection ~ 7950 4500
Wire Wire Line
	7950 4500 7450 4500
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C7
U 1 1 5D548555
P 4600 4550
AR Path="/5D820ACD/5D548555" Ref="C7"  Part="1" 
AR Path="/5D8BF7DF/5D548555" Ref="C13"  Part="1" 
F 0 "C7" H 4715 4596 50  0000 L CNN
F 1 "2.2uF" H 4715 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4638 4400 50  0001 C CNN
F 3 "~" H 4600 4550 50  0001 C CNN
	1    4600 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4400 4600 4350
Wire Wire Line
	4600 4350 5000 4350
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR025
U 1 1 5D54D494
P 4600 4750
AR Path="/5D820ACD/5D54D494" Ref="#PWR025"  Part="1" 
AR Path="/5D8BF7DF/5D54D494" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 4600 4500 50  0001 C CNN
F 1 "GND" H 4605 4577 50  0000 C CNN
F 2 "" H 4600 4750 50  0001 C CNN
F 3 "" H 4600 4750 50  0001 C CNN
	1    4600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4700 4600 4750
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R2
U 1 1 5D5574E1
P 3000 3050
AR Path="/5D820ACD/5D5574E1" Ref="R2"  Part="1" 
AR Path="/5D8BF7DF/5D5574E1" Ref="R13"  Part="1" 
F 0 "R2" V 3080 3050 50  0000 C CNN
F 1 "10K" V 3000 2975 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2930 3050 50  0001 C CNN
F 3 "~" H 3000 3050 50  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R11
U 1 1 5D55943F
P 4800 2800
AR Path="/5D820ACD/5D55943F" Ref="R11"  Part="1" 
AR Path="/5D8BF7DF/5D55943F" Ref="R22"  Part="1" 
F 0 "R11" V 4880 2800 50  0000 C CNN
F 1 "10K" V 4800 2725 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4730 2800 50  0001 C CNN
F 3 "~" H 4800 2800 50  0001 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
Text Label 4850 3000 0    50   ~ 0
~RST
Text Label 4850 3300 0    50   ~ 0
CLK
Text Label 4850 3400 0    50   ~ 0
CMD
Text Label 4850 3500 0    50   ~ 0
DAT0
Text Label 4850 3600 0    50   ~ 0
DAT1
Text Label 4850 3700 0    50   ~ 0
DAT2
Text Label 4850 3800 0    50   ~ 0
DAT3
Text Label 4850 3900 0    50   ~ 0
DAT4
Text Label 4850 4000 0    50   ~ 0
DAT5
Text Label 4850 4100 0    50   ~ 0
DAT6
Text Label 4850 4200 0    50   ~ 0
DAT7
Text Notes 4350 2200 0    100  ~ 0
eMMC v4.3 with eMMC v5.x device
Text HLabel 2850 3500 0    50   BiDi ~ 0
DAT0
Text HLabel 2850 3600 0    50   BiDi ~ 0
DAT1
Text HLabel 2850 3700 0    50   BiDi ~ 0
DAT2
Text HLabel 2850 3800 0    50   BiDi ~ 0
DAT3
Text HLabel 2850 3900 0    50   BiDi ~ 0
DAT4
Text HLabel 2850 4000 0    50   BiDi ~ 0
DAT5
Text HLabel 2850 4100 0    50   BiDi ~ 0
DAT6
Text HLabel 2850 4200 0    50   BiDi ~ 0
DAT7
Text HLabel 2650 3400 0    50   Input ~ 0
CMD
Text HLabel 2450 3300 0    50   Input ~ 0
CLK
Text Notes 650  4150 0    50   ~ 0
Routing Constraints\n– Mismatch within DAT0-DAT7 : 6.35 mm \n– CLK to DAT0-7 mismatch : 6.35 mm\n– CLK to CMD mismatch : 6.35 mm\n– CLK to RST_N mismatch : 25.4 mm
$Comp
L beagle-cube:SDINBDG4-XXG-ZA U10
U 1 1 5D7D520A
P 5700 4300
AR Path="/5D820ACD/5D7D520A" Ref="U10"  Part="1" 
AR Path="/5D8BF7DF/5D7D520A" Ref="U11"  Part="1" 
F 0 "U10" H 5700 5867 50  0000 C CNN
F 1 "SDINBDG4-XXG-ZA" H 5700 5776 50  0000 C CNN
F 2 "footprints:EMMC_BGA" H 4600 5650 50  0001 C CNN
F 3 "" H 4950 5750 50  0001 C CNN
	1    5700 4300
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:R_Pack04-Device-beagle-cube-rescue RN2
U 1 1 5E58D933
P 4450 3000
AR Path="/5D820ACD/5E58D933" Ref="RN2"  Part="1" 
AR Path="/5D8BF7DF/5E58D933" Ref="RN4"  Part="1" 
F 0 "RN2" H 4638 3046 50  0000 L CNN
F 1 "10K" H 4638 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Concave_4x0603" V 4725 3000 50  0001 C CNN
F 3 "~" H 4450 3000 50  0001 C CNN
	1    4450 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2600 3450 2600
$Comp
L beagle-cube-rescue:R_Pack04-Device-beagle-cube-rescue RN1
U 1 1 5E59ABC7
P 3650 3000
AR Path="/5D820ACD/5E59ABC7" Ref="RN1"  Part="1" 
AR Path="/5D8BF7DF/5E59ABC7" Ref="RN3"  Part="1" 
F 0 "RN1" H 3838 3046 50  0000 L CNN
F 1 "10K" H 3838 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Concave_4x0603" V 3925 3000 50  0001 C CNN
F 3 "~" H 3650 3000 50  0001 C CNN
	1    3650 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3300 2800 3300
Wire Wire Line
	2650 3400 3000 3400
Wire Wire Line
	4550 3200 4550 4200
Wire Wire Line
	3000 3200 3000 3400
Connection ~ 3000 3400
Wire Wire Line
	3000 3400 5000 3400
Wire Wire Line
	2800 3200 2800 3300
Connection ~ 2800 3300
Wire Wire Line
	2800 3300 5000 3300
Wire Wire Line
	3450 2800 3450 2600
Connection ~ 3450 2600
Wire Wire Line
	3450 2600 3550 2600
Wire Wire Line
	3550 2800 3550 2600
Connection ~ 3550 2600
Wire Wire Line
	3550 2600 3650 2600
Wire Wire Line
	3650 2800 3650 2600
Connection ~ 3650 2600
Wire Wire Line
	3650 2600 3750 2600
Wire Wire Line
	3750 2800 3750 2600
Connection ~ 3750 2600
Wire Wire Line
	4250 2800 4250 2600
Wire Wire Line
	4350 2800 4350 2600
Connection ~ 4250 2600
Wire Wire Line
	4250 2600 4350 2600
Wire Wire Line
	4450 2800 4450 2600
Connection ~ 4350 2600
Wire Wire Line
	4350 2600 4450 2600
Wire Wire Line
	4550 2800 4550 2600
Connection ~ 4450 2600
Wire Wire Line
	4450 2600 4550 2600
Wire Wire Line
	2850 3500 4250 3500
Connection ~ 4550 2600
Wire Wire Line
	4550 2600 4800 2600
Connection ~ 4550 4200
Wire Wire Line
	4550 4200 5000 4200
Wire Wire Line
	3750 2600 4250 2600
Wire Wire Line
	4450 3200 4450 3700
Connection ~ 4450 3700
Wire Wire Line
	4450 3700 5000 3700
Wire Wire Line
	4350 3200 4350 3600
Connection ~ 4350 3600
Wire Wire Line
	4350 3600 5000 3600
Wire Wire Line
	4250 3200 4250 3500
Connection ~ 4250 3500
Wire Wire Line
	4250 3500 5000 3500
Wire Wire Line
	3750 3200 3750 3900
Connection ~ 3750 3900
Wire Wire Line
	3750 3900 5000 3900
Wire Wire Line
	3650 3200 3650 3800
Wire Wire Line
	3650 3800 5000 3800
Wire Wire Line
	3550 3200 3550 4000
Wire Wire Line
	3450 3200 3450 4100
Wire Wire Line
	2850 3600 4350 3600
Wire Wire Line
	2850 3700 4450 3700
Wire Wire Line
	2850 3900 3750 3900
Wire Wire Line
	3650 3800 2850 3800
Connection ~ 3650 3800
Wire Wire Line
	2850 4000 3550 4000
Connection ~ 3550 4000
Wire Wire Line
	3550 4000 5000 4000
Wire Wire Line
	2850 4100 3450 4100
Connection ~ 3450 4100
Wire Wire Line
	3450 4100 5000 4100
Wire Wire Line
	2850 4200 4550 4200
$EndSCHEMATC
