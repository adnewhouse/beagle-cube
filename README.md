# Beagle-Cube

The goal of this project is to create a reliable and compact CubeSat flight computer prototype based on the popular Pocket Beagle/Beagle Bone Black SBCs. 


## Current Status
- Schematic capture phase in progress

<img src="render.PNG"  width="300">

### Feature List
- Dual CAN bus
- Dual USB (1x OTG, 1x Host)
- 4x RS-422 interfaces (1x with HW flow control)
- Onboard magnetometer and 6-axis IMU
- Dual I2C interfaces
- Dual EMMCs (8 bit bus)
- MicroSD Card
- Real time clock
