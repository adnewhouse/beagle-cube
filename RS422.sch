EESchema Schematic File Version 4
LIBS:beagle-cube-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 8
Title "UART to RS-422"
Date "2019-09-03"
Rev "A1"
Comp "Adam Newhouse"
Comment1 "Copyright 2019"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5150 4350 0    50   Input ~ 0
D
Wire Wire Line
	5150 4350 5250 4350
Text HLabel 5150 4100 0    50   Output ~ 0
R
Wire Wire Line
	5150 4100 5250 4100
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR015
U 1 1 5D72D260
P 5600 4800
AR Path="/5D7285C5/5D72D260" Ref="#PWR015"  Part="1" 
AR Path="/5D77CE26/5D72D260" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 5600 4550 50  0001 C CNN
F 1 "GND" H 5605 4627 50  0000 C CNN
F 2 "" H 5600 4800 50  0001 C CNN
F 3 "" H 5600 4800 50  0001 C CNN
	1    5600 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4750 5600 4800
$Comp
L beagle-cube-rescue:+3V3-power-beagle-cube-rescue #PWR014
U 1 1 5D72D995
P 5600 3650
AR Path="/5D7285C5/5D72D995" Ref="#PWR014"  Part="1" 
AR Path="/5D77CE26/5D72D995" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 5600 3500 50  0001 C CNN
F 1 "+3V3" H 5615 3823 50  0000 C CNN
F 2 "" H 5600 3650 50  0001 C CNN
F 3 "" H 5600 3650 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3650 5600 3700
Text HLabel 6850 3350 2    50   Input ~ 0
A
Wire Wire Line
	6050 4050 5950 4050
Text HLabel 6900 4150 2    50   Input ~ 0
B
Text HLabel 6850 5100 2    50   Output ~ 0
Z
Text HLabel 6900 4300 2    50   Output ~ 0
Y
Wire Wire Line
	5950 4400 6050 4400
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C5
U 1 1 5D72EA3A
P 4600 3800
AR Path="/5D7285C5/5D72EA3A" Ref="C5"  Part="1" 
AR Path="/5D77CE26/5D72EA3A" Ref="C6"  Part="1" 
F 0 "C6" H 4715 3846 50  0000 L CNN
F 1 "100 nF" H 4715 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4638 3650 50  0001 C CNN
F 3 "~" H 4600 3800 50  0001 C CNN
	1    4600 3800
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:+3V3-power-beagle-cube-rescue #PWR012
U 1 1 5D72FC80
P 4600 3600
AR Path="/5D7285C5/5D72FC80" Ref="#PWR012"  Part="1" 
AR Path="/5D77CE26/5D72FC80" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 4600 3450 50  0001 C CNN
F 1 "+3V3" H 4615 3773 50  0000 C CNN
F 2 "" H 4600 3600 50  0001 C CNN
F 3 "" H 4600 3600 50  0001 C CNN
	1    4600 3600
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR013
U 1 1 5D72FFE1
P 4600 4000
AR Path="/5D7285C5/5D72FFE1" Ref="#PWR013"  Part="1" 
AR Path="/5D77CE26/5D72FFE1" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 4600 3750 50  0001 C CNN
F 1 "GND" H 4605 3827 50  0000 C CNN
F 2 "" H 4600 4000 50  0001 C CNN
F 3 "" H 4600 4000 50  0001 C CNN
	1    4600 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3950 4600 4000
Wire Wire Line
	4600 3600 4600 3650
$Comp
L beagle-cube-rescue:D_TVS_x2_AAC-Device-beagle-cube-rescue D3
U 1 1 5DD7520E
P 6800 3750
AR Path="/5D77CE26/5DD7520E" Ref="D3"  Part="1" 
AR Path="/5D7285C5/5DD7520E" Ref="D1"  Part="1" 
F 0 "D3" V 6846 3829 50  0000 L CNN
F 1 "SDCAN24-2BLY" V 6755 3829 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6650 3750 50  0001 C CNN
F 3 "~" H 6650 3750 50  0001 C CNN
	1    6800 3750
	0    -1   -1   0   
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR016
U 1 1 5DD7D55A
P 7000 3800
AR Path="/5D7285C5/5DD7D55A" Ref="#PWR016"  Part="1" 
AR Path="/5D77CE26/5DD7D55A" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 7000 3550 50  0001 C CNN
F 1 "GND" H 7005 3627 50  0000 C CNN
F 2 "" H 7000 3800 50  0001 C CNN
F 3 "" H 7000 3800 50  0001 C CNN
	1    7000 3800
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR017
U 1 1 5DD7DC0E
P 7000 4750
AR Path="/5D7285C5/5DD7DC0E" Ref="#PWR017"  Part="1" 
AR Path="/5D77CE26/5DD7DC0E" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 7000 4500 50  0001 C CNN
F 1 "GND" H 7005 4577 50  0000 C CNN
F 2 "" H 7000 4750 50  0001 C CNN
F 3 "" H 7000 4750 50  0001 C CNN
	1    7000 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4700 7000 4700
Wire Wire Line
	7000 4700 7000 4750
Wire Wire Line
	5950 4150 6800 4150
Wire Wire Line
	5950 4300 6800 4300
Wire Wire Line
	6800 4300 6800 4350
Connection ~ 6800 4300
Wire Wire Line
	6800 4300 6900 4300
Wire Wire Line
	6800 4150 6800 4100
Connection ~ 6800 4150
Wire Wire Line
	6800 4150 6900 4150
Wire Wire Line
	6050 4050 6050 3350
Wire Wire Line
	6050 3350 6800 3350
Wire Wire Line
	6050 4400 6050 5100
Wire Wire Line
	6050 5100 6800 5100
Wire Wire Line
	6800 5050 6800 5100
Connection ~ 6800 5100
Wire Wire Line
	6800 5100 6850 5100
Wire Wire Line
	6800 3400 6800 3350
Connection ~ 6800 3350
Wire Wire Line
	6800 3350 6850 3350
Wire Wire Line
	6950 3750 7000 3750
Wire Wire Line
	7000 3750 7000 3800
$Comp
L beagle-cube:SN65HVD74 U8
U 1 1 5D7DCAA2
P 5600 4100
AR Path="/5D7285C5/5D7DCAA2" Ref="U8"  Part="1" 
AR Path="/5D77CE26/5D7DCAA2" Ref="U9"  Part="1" 
F 0 "U9" H 5800 4450 50  0000 C CNN
F 1 "SN65HVD74" H 5300 4450 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 5600 4100 50  0001 C CNN
F 3 "" H 5600 4100 50  0001 C CNN
	1    5600 4100
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:D_TVS_x2_AAC-Device-beagle-cube-rescue D4
U 1 1 5DD789F9
P 6800 4700
AR Path="/5D77CE26/5DD789F9" Ref="D4"  Part="1" 
AR Path="/5D7285C5/5DD789F9" Ref="D2"  Part="1" 
F 0 "D4" V 6754 4779 50  0000 L CNN
F 1 "SDCAN24-2BLY" V 6845 4779 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6650 4700 50  0001 C CNN
F 3 "~" H 6650 4700 50  0001 C CNN
	1    6800 4700
	0    -1   1    0   
$EndComp
$EndSCHEMATC
