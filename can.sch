EESchema Schematic File Version 4
LIBS:beagle-cube-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L beagle-cube-rescue:TCAN332-Interface_CAN_LIN-beagle-cube-rescue U?
U 1 1 5D8AC4D4
P 6050 4200
AR Path="/5D8AC4D4" Ref="U?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC4D4" Ref="U5"  Part="1" 
AR Path="/5D8D481F/5D8AC4D4" Ref="U6"  Part="1" 
F 0 "U6" H 6400 4550 50  0000 C CNN
F 1 "TCAN332" H 6050 4200 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-8" H 6050 3700 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tcan337.pdf" H 6050 4200 50  0001 C CNN
	1    6050 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR?
U 1 1 5D8AC4DA
P 6050 4650
AR Path="/5D8AC4DA" Ref="#PWR?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC4DA" Ref="#PWR0108"  Part="1" 
AR Path="/5D8D481F/5D8AC4DA" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 6050 4400 50  0001 C CNN
F 1 "GND" H 6055 4477 50  0000 C CNN
F 2 "" H 6050 4650 50  0001 C CNN
F 3 "" H 6050 4650 50  0001 C CNN
	1    6050 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4600 6050 4650
$Comp
L beagle-cube-rescue:R_Small-Device-beagle-cube-rescue R?
U 1 1 5D8AC4E1
P 6700 4200
AR Path="/5D8AC4E1" Ref="R?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC4E1" Ref="R23"  Part="1" 
AR Path="/5D8D481F/5D8AC4E1" Ref="R24"  Part="1" 
F 0 "R24" H 6641 4154 50  0000 R CNN
F 1 "120R" H 6641 4245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6700 4200 50  0001 C CNN
F 3 "~" H 6700 4200 50  0001 C CNN
	1    6700 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 4200 7150 4200
Wire Wire Line
	7100 4250 7100 4200
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR?
U 1 1 5D8AC4E9
P 7100 4250
AR Path="/5D8AC4E9" Ref="#PWR?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC4E9" Ref="#PWR0109"  Part="1" 
AR Path="/5D8D481F/5D8AC4E9" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 7100 4000 50  0001 C CNN
F 1 "GND" H 7105 4077 50  0000 C CNN
F 2 "" H 7100 4250 50  0001 C CNN
F 3 "" H 7100 4250 50  0001 C CNN
	1    7100 4250
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:D_TVS_x2_AAC-Device-beagle-cube-rescue D?
U 1 1 5D8AC4EF
P 7300 4200
AR Path="/5D77CE26/5D8AC4EF" Ref="D?"  Part="1" 
AR Path="/5D7285C5/5D8AC4EF" Ref="D?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC4EF" Ref="D5"  Part="1" 
AR Path="/5D8D481F/5D8AC4EF" Ref="D6"  Part="1" 
F 0 "D6" V 7254 4279 50  0000 L CNN
F 1 "SDCAN24-2BLY" V 7345 4279 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7150 4200 50  0001 C CNN
F 3 "~" H 7150 4200 50  0001 C CNN
	1    7300 4200
	0    1    -1   0   
$EndComp
Wire Wire Line
	6550 4100 6600 4100
Wire Wire Line
	6600 4100 6600 4050
Wire Wire Line
	6600 4050 6700 4050
Wire Wire Line
	6700 4050 6700 4100
Wire Wire Line
	6700 4050 6900 4050
Wire Wire Line
	6900 4050 6900 3800
Wire Wire Line
	6900 3800 7300 3800
Wire Wire Line
	7300 3800 7300 3850
Connection ~ 6700 4050
Wire Wire Line
	7300 3800 7400 3800
Connection ~ 7300 3800
Wire Wire Line
	6550 4300 6600 4300
Wire Wire Line
	6600 4300 6600 4350
Wire Wire Line
	6600 4350 6700 4350
Wire Wire Line
	6700 4350 6700 4300
Wire Wire Line
	6700 4350 6900 4350
Wire Wire Line
	6900 4350 6900 4600
Wire Wire Line
	6900 4600 7300 4600
Wire Wire Line
	7300 4600 7300 4550
Connection ~ 6700 4350
Wire Wire Line
	7300 4600 7400 4600
Connection ~ 7300 4600
$Comp
L beagle-cube-rescue:+3V3-power-beagle-cube-rescue #PWR?
U 1 1 5D8AC50B
P 6050 3750
AR Path="/5D8AC50B" Ref="#PWR?"  Part="1" 
AR Path="/5D8A8FF7/5D8AC50B" Ref="#PWR0110"  Part="1" 
AR Path="/5D8D481F/5D8AC50B" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 6050 3600 50  0001 C CNN
F 1 "+3V3" H 6065 3923 50  0000 C CNN
F 2 "" H 6050 3750 50  0001 C CNN
F 3 "" H 6050 3750 50  0001 C CNN
	1    6050 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3750 6050 3800
Text HLabel 7400 3800 2    50   BiDi ~ 0
CANH
Text HLabel 7400 4600 2    50   BiDi ~ 0
CANL
Text HLabel 5500 4000 0    50   Output ~ 0
CAN_TX
Text HLabel 5500 4100 0    50   Input ~ 0
CAN_RX
Wire Wire Line
	5500 4100 5550 4100
Wire Wire Line
	5500 4000 5550 4000
$Comp
L beagle-cube-rescue:C-Device-beagle-cube-rescue C19
U 1 1 5D8AD3D5
P 4650 4200
AR Path="/5D8A8FF7/5D8AD3D5" Ref="C19"  Part="1" 
AR Path="/5D8D481F/5D8AD3D5" Ref="C20"  Part="1" 
F 0 "C20" H 4765 4246 50  0000 L CNN
F 1 "100nF" H 4765 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4688 4050 50  0001 C CNN
F 3 "~" H 4650 4200 50  0001 C CNN
	1    4650 4200
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:+3V3-power-beagle-cube-rescue #PWR?
U 1 1 5D8ADF28
P 4650 4000
AR Path="/5D8ADF28" Ref="#PWR?"  Part="1" 
AR Path="/5D8A8FF7/5D8ADF28" Ref="#PWR0111"  Part="1" 
AR Path="/5D8D481F/5D8ADF28" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 4650 3850 50  0001 C CNN
F 1 "+3V3" H 4665 4173 50  0000 C CNN
F 2 "" H 4650 4000 50  0001 C CNN
F 3 "" H 4650 4000 50  0001 C CNN
	1    4650 4000
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR?
U 1 1 5D8AEF9E
P 4650 4400
AR Path="/5D8AEF9E" Ref="#PWR?"  Part="1" 
AR Path="/5D8A8FF7/5D8AEF9E" Ref="#PWR0112"  Part="1" 
AR Path="/5D8D481F/5D8AEF9E" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 4650 4150 50  0001 C CNN
F 1 "GND" H 4655 4227 50  0000 C CNN
F 2 "" H 4650 4400 50  0001 C CNN
F 3 "" H 4650 4400 50  0001 C CNN
	1    4650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4350 4650 4400
Wire Wire Line
	4650 4000 4650 4050
$EndSCHEMATC
