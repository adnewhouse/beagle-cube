EESchema Schematic File Version 4
LIBS:beagle-cube-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L beagle-cube:LAN8710A U7
U 1 1 5D8713A7
P 5950 3800
AR Path="/5E77A61C/5D8713A7" Ref="U7"  Part="1" 
AR Path="/5D87FAC2/5D8713A7" Ref="U?"  Part="1" 
F 0 "U7" H 5250 5200 50  0000 C CNN
F 1 "LAN8710A" H 5950 3800 50  0000 C CNN
F 2 "" H 5250 5200 50  0001 C CNN
F 3 "" H 5250 5200 50  0001 C CNN
	1    5950 3800
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R9
U 1 1 5D913337
P 4000 3100
F 0 "R9" V 3950 3300 50  0000 C CNN
F 1 "100R" V 3950 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3100 50  0001 C CNN
F 3 "~" H 4000 3100 50  0001 C CNN
	1    4000 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3100 5000 3100
Wire Wire Line
	3850 3100 3500 3100
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R10
U 1 1 5D920BB0
P 4000 3200
F 0 "R10" V 3950 3400 50  0000 C CNN
F 1 "100R" V 3950 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3200 50  0001 C CNN
F 3 "~" H 4000 3200 50  0001 C CNN
	1    4000 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3200 5000 3200
Wire Wire Line
	3850 3200 3500 3200
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R14
U 1 1 5D92120F
P 4000 3300
F 0 "R14" V 3950 3500 50  0000 C CNN
F 1 "100R" V 3950 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3300 50  0001 C CNN
F 3 "~" H 4000 3300 50  0001 C CNN
	1    4000 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3300 5000 3300
Wire Wire Line
	3850 3300 3500 3300
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R15
U 1 1 5D92121B
P 4000 3400
F 0 "R15" V 3950 3600 50  0000 C CNN
F 1 "100R" V 3950 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3400 50  0001 C CNN
F 3 "~" H 4000 3400 50  0001 C CNN
	1    4000 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3400 5000 3400
Wire Wire Line
	3850 3400 3500 3400
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R16
U 1 1 5D921F55
P 4000 3500
F 0 "R16" V 3950 3700 50  0000 C CNN
F 1 "100R" V 3950 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3500 50  0001 C CNN
F 3 "~" H 4000 3500 50  0001 C CNN
	1    4000 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3500 5000 3500
Wire Wire Line
	3850 3500 3500 3500
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R17
U 1 1 5D921F61
P 4000 3600
F 0 "R17" V 3950 3800 50  0000 C CNN
F 1 "100R" V 3950 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3600 50  0001 C CNN
F 3 "~" H 4000 3600 50  0001 C CNN
	1    4000 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3600 5000 3600
Wire Wire Line
	3850 3600 3500 3600
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R18
U 1 1 5D921F6D
P 4000 3700
F 0 "R18" V 3950 3900 50  0000 C CNN
F 1 "100R" V 3950 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3700 50  0001 C CNN
F 3 "~" H 4000 3700 50  0001 C CNN
	1    4000 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3700 5000 3700
Wire Wire Line
	3850 3700 3500 3700
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R19
U 1 1 5D921F79
P 4000 4400
F 0 "R19" V 3950 4600 50  0000 C CNN
F 1 "100R" V 3950 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 4400 50  0001 C CNN
F 3 "~" H 4000 4400 50  0001 C CNN
	1    4000 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 4400 5000 4400
Wire Wire Line
	3850 4400 3500 4400
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R20
U 1 1 5D925BE6
P 4000 5000
F 0 "R20" V 3950 5200 50  0000 C CNN
F 1 "100R" V 3950 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 5000 50  0001 C CNN
F 3 "~" H 4000 5000 50  0001 C CNN
	1    4000 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 5000 5000 5000
Wire Wire Line
	3850 5000 3500 5000
$Comp
L beagle-cube-rescue:R-Device-beagle-cube-rescue R21
U 1 1 5D925BF2
P 4000 5100
F 0 "R21" V 3950 5300 50  0000 C CNN
F 1 "100R" V 3950 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 5100 50  0001 C CNN
F 3 "~" H 4000 5100 50  0001 C CNN
	1    4000 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 5100 5000 5100
Wire Wire Line
	3850 5100 3500 5100
Wire Wire Line
	5000 3900 3500 3900
Wire Wire Line
	5000 4000 3500 4000
Wire Wire Line
	5000 4100 3500 4100
Wire Wire Line
	5000 4200 3500 4200
Wire Wire Line
	5000 4300 3500 4300
Wire Wire Line
	5000 4700 3500 4700
Wire Wire Line
	5000 4800 3500 4800
$Comp
L beagle-cube-rescue:Crystal-Device-beagle-cube-rescue Y1
U 1 1 5D92A70D
P 7450 4450
F 0 "Y1" H 7450 4718 50  0000 C CNN
F 1 "Crystal" H 7450 4627 50  0000 C CNN
F 2 "" H 7450 4450 50  0001 C CNN
F 3 "~" H 7450 4450 50  0001 C CNN
	1    7450 4450
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:C_Small-Device-beagle-cube-rescue C21
U 1 1 5D92D3DD
P 7700 4650
F 0 "C21" H 7792 4696 50  0000 L CNN
F 1 "C_Small" H 7792 4605 50  0000 L CNN
F 2 "" H 7700 4650 50  0001 C CNN
F 3 "~" H 7700 4650 50  0001 C CNN
	1    7700 4650
	1    0    0    -1  
$EndComp
Text HLabel 3500 3100 0    50   Input ~ 0
RXD0
Text HLabel 3500 3200 0    50   Input ~ 0
RXD1
Text HLabel 3500 3300 0    50   Input ~ 0
RXD2
Text HLabel 3500 3400 0    50   Input ~ 0
RXD3
Text HLabel 3500 3500 0    50   Input ~ 0
RX_ER
Text HLabel 3500 3600 0    50   Input ~ 0
RX_CLK
Text HLabel 3500 3700 0    50   Input ~ 0
RX_DV
Text HLabel 3500 3900 0    50   Input ~ 0
TXD0
Text HLabel 3500 4000 0    50   Input ~ 0
TXD1
Text HLabel 3500 4100 0    50   Input ~ 0
TXD2
Text HLabel 3500 4200 0    50   Input ~ 0
TXD3
Text HLabel 3500 4300 0    50   Input ~ 0
TX_ER
Text HLabel 3500 4400 0    50   Input ~ 0
TX_CLK
Text HLabel 3500 4700 0    50   Input ~ 0
MDC
Text HLabel 3500 4800 0    50   Input ~ 0
MDIO
Text HLabel 3500 5000 0    50   Input ~ 0
CRS
Text HLabel 3500 5100 0    50   Input ~ 0
COL
Text HLabel 3500 4500 0    50   Input ~ 0
TX_EN
Wire Wire Line
	3500 4500 5000 4500
$Comp
L beagle-cube-rescue:C_Small-Device-beagle-cube-rescue C3
U 1 1 5D969C8E
P 7200 4650
F 0 "C3" H 7292 4696 50  0000 L CNN
F 1 "C_Small" H 7292 4605 50  0000 L CNN
F 2 "" H 7200 4650 50  0001 C CNN
F 3 "~" H 7200 4650 50  0001 C CNN
	1    7200 4650
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR0129
U 1 1 5D96BD62
P 7200 4800
F 0 "#PWR0129" H 7200 4550 50  0001 C CNN
F 1 "GND" H 7205 4627 50  0000 C CNN
F 2 "" H 7200 4800 50  0001 C CNN
F 3 "" H 7200 4800 50  0001 C CNN
	1    7200 4800
	1    0    0    -1  
$EndComp
$Comp
L beagle-cube-rescue:GND-power-beagle-cube-rescue #PWR0130
U 1 1 5D96C502
P 7700 4800
F 0 "#PWR0130" H 7700 4550 50  0001 C CNN
F 1 "GND" H 7705 4627 50  0000 C CNN
F 2 "" H 7700 4800 50  0001 C CNN
F 3 "" H 7700 4800 50  0001 C CNN
	1    7700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4750 7700 4800
Wire Wire Line
	7200 4750 7200 4800
Wire Wire Line
	7200 4550 7200 4450
Wire Wire Line
	7200 4450 7300 4450
Wire Wire Line
	7600 4450 7700 4450
Wire Wire Line
	7700 4450 7700 4550
Wire Wire Line
	7200 4450 7200 4250
Wire Wire Line
	7200 4250 6850 4250
Connection ~ 7200 4450
Wire Wire Line
	6850 4150 7700 4150
Wire Wire Line
	7700 4150 7700 4450
Connection ~ 7700 4450
$Comp
L beagle-cube-rescue:Wuerth_7499010121A-Connector-beagle-cube-rescue J5
U 1 1 5D977587
P 9300 3200
F 0 "J5" H 8770 3211 50  0000 R CNN
F 1 "Wuerth_7499010121A" H 8770 3302 50  0000 R CNN
F 2 "Connector_RJ:RJ45_Wuerth_7499010121A_Horizontal" H 9300 2575 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/7499010121A.pdf" H 8885 2965 50  0001 L TNN
	1    9300 3200
	-1   0    0    1   
$EndComp
$EndSCHEMATC
