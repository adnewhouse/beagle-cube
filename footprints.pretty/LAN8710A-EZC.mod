PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
QFN50P500X500X100-33N
$EndINDEX
$MODULE QFN50P500X500X100-33N
Po 0 0 0 15 00000000 00000000 ~~
Li QFN50P500X500X100-33N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -0.80437 -4.76588 1.00546 1.00546 0 0.05 N V 21 "QFN50P500X500X100-33N"
T1 -0.16585 4.76439 1.00515 1.00515 0 0.05 N V 21 "VAL**"
DP 0 0 0 0 4 0 19
Dl 0.200101 -1.24
Dl 1.24 -1.24
Dl 1.24 -0.200101
Dl 0.200101 -0.200101
DP 0 0 0 0 4 0 19
Dl -1.24042 -1.24
Dl -0.2 -1.24
Dl -0.2 -0.200068
Dl -1.24042 -0.200068
DP 0 0 0 0 4 0 19
Dl -1.24194 0.2
Dl -0.2 0.2
Dl -0.2 1.24194
Dl -1.24194 1.24194
DP 0 0 0 0 4 0 19
Dl 0.200022 0.2
Dl 1.24 0.2
Dl 1.24 1.24014
Dl 0.200022 1.24014
DC -3.275 -2.01 -3.175 -2.01 0.2 21
DC -3.275 -2.01 -3.175 -2.01 0.2 27
DS 2.5 2.5 -2.5 2.5 0.127 27
DS 2.5 -2.5 -2.5 -2.5 0.127 27
DS 2.5 2.5 2.5 -2.5 0.127 27
DS -2.5 2.5 -2.5 -2.5 0.127 27
DS 2.5 2.5 2.15 2.5 0.127 21
DS 2.5 -2.5 2.15 -2.5 0.127 21
DS -2.5 2.5 -2.15 2.5 0.127 21
DS -2.5 -2.5 -2.15 -2.5 0.127 21
DS 2.5 2.5 2.5 2.15 0.127 21
DS 2.5 -2.5 2.5 -2.15 0.127 21
DS -2.5 2.5 -2.5 2.15 0.127 21
DS -2.5 -2.5 -2.5 -2.15 0.127 21
DS -3.12 3.12 3.12 3.12 0.05 26
DS -3.12 -3.12 3.12 -3.12 0.05 26
DS -3.12 3.12 -3.12 -3.12 0.05 26
DS 3.12 3.12 3.12 -3.12 0.05 26
$PAD
Sh "9" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 2.435
$EndPAD
$PAD
Sh "10" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 2.435
$EndPAD
$PAD
Sh "11" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 2.435
$EndPAD
$PAD
Sh "12" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 2.435
$EndPAD
$PAD
Sh "13" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 2.435
$EndPAD
$PAD
Sh "14" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 2.435
$EndPAD
$PAD
Sh "15" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 2.435
$EndPAD
$PAD
Sh "16" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 2.435
$EndPAD
$PAD
Sh "25" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 -2.435
$EndPAD
$PAD
Sh "26" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -2.435
$EndPAD
$PAD
Sh "27" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -2.435
$EndPAD
$PAD
Sh "28" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -2.435
$EndPAD
$PAD
Sh "29" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 -2.435
$EndPAD
$PAD
Sh "30" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 -2.435
$EndPAD
$PAD
Sh "31" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 -2.435
$EndPAD
$PAD
Sh "32" R 0.86 0.26 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 -2.435
$EndPAD
$PAD
Sh "1" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 -1.75
$EndPAD
$PAD
Sh "2" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 -1.25
$EndPAD
$PAD
Sh "3" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 -0.75
$EndPAD
$PAD
Sh "4" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 -0.25
$EndPAD
$PAD
Sh "5" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 0.25
$EndPAD
$PAD
Sh "6" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 0.75
$EndPAD
$PAD
Sh "7" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 1.25
$EndPAD
$PAD
Sh "8" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.435 1.75
$EndPAD
$PAD
Sh "17" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 1.75
$EndPAD
$PAD
Sh "18" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 1.25
$EndPAD
$PAD
Sh "19" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 0.75
$EndPAD
$PAD
Sh "20" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 0.25
$EndPAD
$PAD
Sh "21" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 -0.25
$EndPAD
$PAD
Sh "22" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 -0.75
$EndPAD
$PAD
Sh "23" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 -1.25
$EndPAD
$PAD
Sh "24" R 0.86 0.26 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.435 -1.75
$EndPAD
$PAD
Sh "33" R 3.3 3.3 0 0 0
At SMD N 00888000
.SolderMask 0
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE QFN50P500X500X100-33N
